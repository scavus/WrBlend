##########################################################
# Copyright 2016 Sencer Cavus. All rights reserved.      #
# License: https://opensource.org/licenses/MIT           #
##########################################################

class WrPatternGraph(bpy.types.NodeTree):
    bl_idname = 'WrPatternGraph'
    bl_label = 'Wr Pattern Graph'
    bl_icon = 'MATERIAL'
    nodetypes = {}


class WrSocket:
    ui_open = bpy.props.BoolProperty(name='UI Open', default=True)

    # Optional function for drawing the socket input value
    def draw_value(self, context, layout, node):
        layout.prop(node, self.name)

    # green node for color
    def draw_color(self, context, node):
        return (0.1, 0.1, 0.1, 0.75)

    #draw socket property in node
    def draw(self, context, layout, node, text):
        if self.is_linked or self.is_output:
            layout.label(text)
        else:
            row = layout.row()
            split = row.split(common.label_percentage)
            split.label(text)
            split.prop(node.inputs[text],'default_value',text="")

    def is_empty_socket(self):
        return False


class WrShaderSocket(bpy.types.NodeSocketShader, WrSocket):
    bl_idname = 'WrShaderSocket'
    bl_label = 'Wr Shader Socket'
    default_value = None

    def draw_color(self, context, node):
        return (1.0, 1.0, 0.2, 1.0)

    def IsEmptySocket(self):
        return True


class WrNodeSocketColor(bpy.types.NodeSocketColor, WrSocket):
    bl_idname = 'WrNodeSocketColor'
    bl_label = 'Wr Color Socket'

    default_value = bpy.props.FloatVectorProperty(name = '', default = (1.0, 1.0, 1.0), subtype = 'COLOR', soft_min = 0.0, soft_max = 1.0)

    def draw_color(self, context, node):
        return (0.1, 1.0, 0.2, 1.0)

    def output_default_value_to_str(self):
        return '%f %f %f'%(self.default_value[0],self.default_value[1],self.default_value[2])

    def output_type_str(self):
        return 'color'


class WrNodeBaseColorSocket(bpy.types.NodeSocketColor, WrSocket):
    bl_idname = 'WrNodeBaseColorSocket'
    bl_label = 'Wr Base Color Socket'

    default_value = bpy.props.FloatVectorProperty( name='BaseColor' , default=(1.0, 1.0, 1.0) ,subtype='COLOR',soft_min = 0.0, soft_max = 1.0)

    def draw_color(self, context, node):
        return (0.1, 1.0, 0.2, 1.0)

    def output_default_value_to_str(self):
        return '%f %f %f'%(self.default_value[0],self.default_value[1],self.default_value[2])

    def output_type_str(self):
        return 'color'


class WrNodeRoughnessSocket(bpy.types.NodeSocketFloat, WrSocket):
    bl_idname = 'WrNodeRoughnessSocket'
    bl_label = 'Wr Roughness Socket'

    default_value = bpy.props.FloatProperty( name='Roughness' , default=0.0 , min=0.0, max=1.0 )

    def draw_color(self, context, node):
        return (0.1, 0.1, 0.3, 1.0)

    def output_default_value_to_str(self):
        return '%f'%(self.default_value)

    def output_type_str(self):
        return 'float'


class WrNodeIorSocket(bpy.types.NodeSocketFloat, WrSocket):
    bl_idname = 'WrNodeIorSocket'
    bl_label = 'Wr Index of reafraction Socket'

    default_value = bpy.props.FloatProperty( name='Roughness' , default=1.0 , min=0.0)

    def draw_color(self, context, node):
        return (0.1, 0.1, 0.3, 1.0)

    def output_default_value_to_str(self):
        return '%f'%(self.default_value)

    def output_type_str(self):
        return 'float'


class WrShadingNode(bpy.types.Node):
    bl_label = 'ShadingNode'
    bl_idname = 'WrShadingNode'
    bl_icon = 'MATERIAL'


class WrNodeOutput(WrShadingNode):
    bl_label = 'wr_output'

    def init(self, context):
        input = self.inputs.new('WrNodeSocketColor', 'Surface')


class WrNodeLambert(WrShadingNode):
    bl_label = 'wr_lambert'
    bl_idname = 'WrNodeLambert'

    def init(self, context):
        self.inputs.new('WrNodeBaseColorSocket', 'BaseColor')
        self.outputs.new('WrNodeSocketColor', 'Result')


class WrNodeMicrofacetGgx(WrShadingNode):
    bl_label = 'wr_lambert'
    bl_idname = 'WrNodeMicrofacetGgx'

    def init(self, context):
        self.inputs.new('WrNodeRoughnessSocket', 'Roughness')
        self.inputs.new('WrNodeIorSocket', 'Ior')
        self.inputs.new('WrNodeBaseColorSocket', 'BaseColor')
        self.outputs.new('WrNodeSocketColor', 'Result')


'''
class WrNodeLayerStack(WrShadingNode):
    bl_label = 'wr_lambert'
    bl_idname = 'WrNodeLayerStack'

    def init(self, context):
        self.inputs.new('WrNodeBsdfSocket', 'Layer0')
        self.inputs.new('WrNodeBsdfSocket', 'Layer1')
        self.inputs.new('WrNodeBsdfSocket', 'Layer2')
        self.inputs.new('WrNodeBsdfSocket', 'Layer3')
        self.inputs.new('WrNodeBsdfSocket', 'Layer4')
        self.inputs.new('WrNodeMaskSocket', 'Mask0')
        self.inputs.new('WrNodeMaskSocket', 'Mask1')
        self.inputs.new('WrNodeMaskSocket', 'Mask2')
        self.inputs.new('WrNodeMaskSocket', 'Mask3')
        self.inputs.new('WrNodeMaskSocket', 'Mask4')
        self.outputs.new('WrNodeSocketColor', 'Result')
'''
