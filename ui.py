##########################################################
# Copyright 2016 Sencer Cavus. All rights reserved.      #
# License: https://opensource.org/licenses/MIT           #
##########################################################

import subprocess
import io
import bpy
import bl_ui
from extensions_framework import declarative_property_group
from . import common
from . import preferences
from . import WrAddon
from . import cuda_device_list
from . import ocl_device_list

camera_types = [('PINHOLE', 'Pinhole', '', 0),
                ('THINLENS', 'ThinLens', '', 1)]

@WrAddon.addon_register_class
class WrCamera(declarative_property_group):
    ef_attach_to = ['Camera']
    controls = []
    visibility = {}
    properties = [
        {
            'type': 'enum',
            'attr': 'camera_type',
            'items': camera_types,
            'description': 'Type of the camera',
            'default': 'PINHOLE',
            'save_in_preset': True
        },
        {
            'type': 'float',
            'attr': 'fov',
            'name': 'Fov Angle',
            'description': 'Field of view angle',
            'default': 60.0,
            'min': 0.0,
            'soft_min': 0.0,
            'max': 180.0,
            'soft_max': 180.0,
            'save_in_preset': True
        }
    ]


class WrCameraPanel(bl_ui.properties_data_camera.CameraButtonsPanel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'data'
    COMPAT_ENGINES = {common.default_bl_name}

    @classmethod
    def poll(cls, context):
        rd = context.scene.render
        return super().poll(context) and rd.engine in cls.COMPAT_ENGINES


class CameraPanel(WrCameraPanel, bpy.types.Panel):
    bl_label = 'Lamp Property'

    def draw(self, context):
        camera = context.camera
        layout = self.layout
        layout.prop(camera.WrCamera, 'camera_type')
        layout.prop(camera.WrCamera, 'fov')


area_light_types = [('DISK', 'Disk', '', 0),
                    ('SPHERE', 'Sphere', '', 1),
                    ('QUAD', 'Quad', '', 2)]


@WrAddon.addon_register_class
class WrLamp(declarative_property_group):
    ef_attach_to = ['Lamp']
    controls = []
    visibility = {}
    properties = [
        {
            'type': 'float',
            'attr': 'intensity',
            'name': 'Intensity',
            'description': 'Intensity of emitted light',
            'default': 1.0,
            'min': 0.0,
            'soft_min': 0.0,
            'max': 180.0,
            'soft_max': 180.0,
            'save_in_preset': True
        },
        {
            'type': 'float',
            'attr': 'exposure',
            'name': 'Exposure',
            'description': 'Exposure of emitted light',
            'default': 1.0,
            'min': 0.0,
            'soft_min': 0.0,
            'max': 180.0,
            'soft_max': 180.0,
            'save_in_preset': True
        }
    ]


@WrAddon.addon_register_class
class WrLampArea(declarative_property_group):
    ef_attach_to = ['WrLamp']
    controls = []
    properties = [
        {
            'type': 'enum',
            'attr': 'shape',
            'items': area_light_types,
            'description': 'Shape of the area light',
            'default': 'QUAD',
            'save_in_preset': True
        }
    ]


@WrAddon.addon_register_class
class WrLampEnv(declarative_property_group):
    ef_attach_to = ['WrLamp']
    controls = []
    properties = [
        {
            'type': 'string',
            'subtype': 'FILE_PATH',
            'attr': 'envmap_file',
            'name': 'HDRI Map',
            'description': 'EXR image to use for lighting (in latitude-longitude format)',
            'default': '',
            'save_in_preset': True
        }
    ]


class WrLampPanel(bl_ui.properties_data_lamp.DataButtonsPanel):
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "data"
    COMPAT_ENGINES = {common.default_bl_name}

    @classmethod
    def poll(cls, context):
        rd = context.scene.render
        return super().poll(context) and rd.engine in cls.COMPAT_ENGINES


class LampPanel(WrLampPanel, bpy.types.Panel):
    bl_label = 'Lamp Property'

    def draw(self, context):
        lamp = context.lamp

        if lamp is not None:
            layout = self.layout
            layout.prop(lamp, "type", expand=True)
            layout.prop(lamp, "color")
            layout.prop(lamp.WrLamp, 'intensity')
            layout.prop(lamp.WrLamp, 'exposure')


class LampAreaPanel(WrLampPanel, bpy.types.Panel):
    bl_label = 'Area Light Properties'

    @classmethod
    def poll(cls, context):
        return super().poll(context) and context.lamp.type == 'AREA'

    def draw(self, context):
        layout = self.layout
        lamp = context.lamp
        layout.prop(lamp.WrLamp.WrLampArea, 'shape', text='Shape')


class LampEnvPanel(WrLampPanel, bpy.types.Panel):
    bl_label = 'Env Light Properties'

    @classmethod
    def poll(cls, context):
        return super().poll(context) and context.lamp.type == 'HEMI'

    def draw(self, context):
        layout = self.layout
        lamp = context.lamp
        layout.prop(lamp.WrLamp.WrLampEnv, 'envmap_file', text='HDRI file')


class WrRenderPanel:
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "render"
    COMPAT_ENGINES = {common.default_bl_name}

    @classmethod
    def poll(cls, context):
        rd = context.scene.render
        return rd.engine in cls.COMPAT_ENGINES


class DeviceRefreshOperator(bpy.types.Operator):
    bl_idname = 'obj.refresh_device_list_operator'
    bl_label = 'Refresh Device List'

    def execute(self, context):
        # TODO: Cache device lists
        p = subprocess.Popen([preferences.get_binary_path(), '-d', 'cuda'], stdout=subprocess.PIPE)
        o = p.communicate()[0]
        buf = io.StringIO(o.decode('utf-8'))
        DevicePanel.c_device_list = buf.readlines()
        bpy.types.Scene.cuda_devs = bpy.props.BoolVectorProperty(name='Cuda Devices', description='Available CUDA device list', size=len(DevicePanel.c_device_list))
        #bpy.types.Scene.cuda_devs = bpy.props.BoolProperty(name='Cuda Devices', description='Available CUDA device list')
        p = subprocess.Popen([preferences.get_binary_path(), '-dl', 'ocl'], stdout=subprocess.PIPE)
        o = p.communicate()[0]
        buf = io.StringIO(o.decode('utf-8'))
        DevicePanel.o_device_list = buf.readlines()
        bpy.types.Scene.ocl_devs = bpy.props.BoolVectorProperty(name='OpenCL Devices', description='Available OpenCL device list', size=len(DevicePanel.o_device_list))
        #bpy.types.Scene.ocl_devs = bpy.props.BoolProperty(name='OpenCL Devices', description='Available OpenCL device list')
        return {'FINISHED'}


class DevicePanel(WrRenderPanel, bpy.types.Panel):
    bl_label = 'Device'
    c_device_list = []
    o_device_list = []

    def draw(self, context):
        layout = self.layout
        layout.operator(DeviceRefreshOperator.bl_idname)

        #row = layout.row()
        #row.prop(scene, "flag_all", text="SELECT ALL")
        #row = layout.row(align=True)
        for i, dev in enumerate(self.c_device_list):
            layout.prop(context.scene, 'cuda_devs', index=i, text=dev)

        for i, dev in enumerate(self.o_device_list):
            layout.prop(context.scene, 'ocl_devs', index=i, text=dev)


class IntegratorPanel(WrRenderPanel, bpy.types.Panel):
    bl_label = common.integrator_panel_bl_name

    integrator_types = [
        ('PROGRESSIVE_DEFERRED_UDPT', 'Progressive Deferred Unidirectional Path Tracer', '', 1),
        ('BRANCHED_DEFERRED_UDPT', 'Branched Deferred Unidirectional Path Tracer', '', 2)
    ]

    bpy.types.Scene.integrator_type_prop = bpy.props.EnumProperty(items=integrator_types, name='Integrator')

    mesh_intersector_types = [
        ('MeshBvh2', 'Default Bvh2 Intersector', '', 1),
        ('FireRays', 'AMD FireRays Intersector', '', 2),
        ('OptixPrime', 'NVIDIA OptiX Prime Intersector', '', 3)
    ]

    area_light_intersector_types = [
        ('AreaLightLinear', 'Default Area Light Intersector', '', 1)
    ]

    bpy.types.Scene.mesh_intersector_type_prop = bpy.props.EnumProperty(items=mesh_intersector_types, name='Mesh Intersector')
    bpy.types.Scene.area_light_intersector_type_prop = bpy.props.EnumProperty(items=area_light_intersector_types, name='Area Light Intersector')

    # progressive deferred udpt settings
    bpy.types.Scene.pdudpt_iteration_count = bpy.props.IntProperty(name='Interation Count', default=100, min=1)
    bpy.types.Scene.pdudpt_max_indirect_bounce_count = bpy.props.IntProperty(name='Max Indirect Illumination Path Depth', default=8, min=0)
    bpy.types.Scene.pdudpt_enable_mis = bpy.props.BoolProperty(name='Enable Multiple Importance Sampling', default=True)
    bpy.types.Scene.pdudpt_enable_rr = bpy.props.BoolProperty(name='Enable Russian Roulette Sampling', default=True)

    # branched deferred udpt settings
    bpy.types.Scene.bdudpt_aa_sample_count = bpy.props.IntProperty(name='Antialiasing Sample Count', default=4, min=1)
    bpy.types.Scene.bdudpt_di_sample_count = bpy.props.IntProperty(name='Direct Illumination Sample Count', default=8, min=1)
    bpy.types.Scene.bdudpt_ii_sample_count = bpy.props.IntProperty(name='Indirect Illumination Sample Count', default=8, min=1)
    bpy.types.Scene.bdudpt_max_indirect_bounce_count = bpy.props.IntProperty(name='Max Indirect Illumination Path Depth', default=8, min=0)
    bpy.types.Scene.bdudpt_enable_mis = bpy.props.BoolProperty(name='Enable Multiple Importance Sampling', default=True)
    bpy.types.Scene.bdudpt_enable_rr = bpy.props.BoolProperty(name='Enable Russian Roulette Sampling', default=True)

    bpy.types.Scene.tile_width = bpy.props.IntProperty(name='Tile Width', default=32, min=1)
    bpy.types.Scene.tile_height = bpy.props.IntProperty(name='Tile Height', default=32, min=1)

    def draw(self, context):
        self.layout.prop(context.scene, 'integrator_type_prop')
        integrator_type = context.scene.integrator_type_prop

        if integrator_type == 'PROGRESSIVE_DEFERRED_UDPT':
            self.layout.prop(context.scene, 'pdudpt_iteration_count')
            self.layout.prop(context.scene, 'pdudpt_max_indirect_bounce_count')
            self.layout.prop(context.scene, 'pdudpt_enable_mis')
            self.layout.prop(context.scene, 'pdudpt_enable_rr')

        if integrator_type == 'BRANCHED_DEFERRED_UDPT':
            self.layout.prop(context.scene, 'bdudpt_aa_sample_count')
            self.layout.prop(context.scene, 'bdudpt_di_sample_count')
            self.layout.prop(context.scene, 'bdudpt_ii_sample_count')
            self.layout.prop(context.scene, 'bdudpt_max_indirect_bounce_count')
            self.layout.prop(context.scene, 'bdudpt_enable_mis')
            self.layout.prop(context.scene, 'bdudpt_enable_rr')

        self.layout.prop(context.scene, 'mesh_intersector_type_prop')
        self.layout.prop(context.scene, 'area_light_intersector_type_prop')
        self.layout.prop(context.scene, 'tile_width')
        self.layout.prop(context.scene, 'tile_height')


def get_panels():
    types = bpy.types
    panels = ['RENDER_PT_render', 'RENDER_PT_dimensions']
    return [getattr(types, p) for p in panels if hasattr(types, p)]


def register():
    bpy.utils.register_class(CameraPanel)
    #bpy.utils.register_class(WrLamp)
    #bpy.utils.register_class(WrLampArea)
    #bpy.utils.register_class(WrLampEnv)
    bpy.utils.register_class(LampPanel)
    bpy.utils.register_class(LampAreaPanel)
    bpy.utils.register_class(LampEnvPanel)
    #bpy.utils.register_class(WrRenderPanel)

    for panel in get_panels():
        panel.COMPAT_ENGINES.add('wrblend')

    bpy.utils.register_class(DeviceRefreshOperator)
    bpy.utils.register_class(DevicePanel)
    bpy.utils.register_class(IntegratorPanel)


def unregister():
    bpy.utils.unregister_class(CameraPanel)
    #bpy.utils.unregister_class(WrLamp)
    #bpy.utils.unregister_class(WrLampArea)
    #bpy.utils.unregister_class(WrLampEnv)
    bpy.utils.unregister_class(LampPanel)
    bpy.utils.unregister_class(LampAreaPanel)
    bpy.utils.unregister_class(LampEnvPanel)
    #bpy.utils.unregister_class(WrRenderPanel)

    for panel in get_panels():
        panel.COMPAT_ENGINES.add('wrblend')

    bpy.utils.unregister_class(DeviceRefreshOperator)
    bpy.utils.unregister_class(DevicePanel)
    bpy.utils.unregister_class(IntegratorPanel)
