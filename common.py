##########################################################
# Copyright 2016 Sencer Cavus. All rights reserved.      #
# License: https://opensource.org/licenses/MIT           #
##########################################################

default_bl_name = 'wrblend'
preferences_bl_name = __package__
integrator_panel_bl_name = 'Integrator'
