##########################################################
# Copyright 2016 Sencer Cavus. All rights reserved.      #
# License: https://opensource.org/licenses/MIT           #
##########################################################

import io
import subprocess
import bpy
from extensions_framework import Addon

bl_info = {
    "name": "Wisard Renderer",
    "description": "GPU-Based Photorealistic Renderer",
    "author": "Sencer Cavus",
    "version": (0, 1, 0),
    "blender": (2, 74, 0),
    "location": "",
    "warning": "Work in progress",
    "category": "Render"}


WrAddon = Addon(bl_info)
addon_register, addon_unregister = WrAddon.init_functions()

cuda_device_list = []
ocl_device_list = []

#bpy.types.Scene.cuda_devs = bpy.props.BoolVectorProperty(name='Cuda Devices', description='Available CUDA device list', size=len(cuda_device_list))
#bpy.types.Scene.ocl_devs = bpy.props.BoolVectorProperty(name='OpenCL Devices', description='Available OpenCL device list', size=len(ocl_device_list))

from . import preferences
from . import engine
from . import ui
#from . import nodes

def register():
    addon_register()
    #bpy.utils.register_module(__name__)
    preferences.register()
    engine.register()
    ui.register()
    #nodes.register()


def unregister():
    addon_unregister()
    #bpy.utils.unregister_module(__name__)
    preferences.unregister()
    engine.unregister()
    ui.unregister()
    #nodes.unregister()
