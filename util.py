##########################################################
# Copyright 2016 Sencer Cavus. All rights reserved.      #
# License: https://opensource.org/licenses/MIT           #
##########################################################

import ctypes
from . import export_types as et

def convert_vec2(v):
    wrv = et.WrVector2f()
    wrv.x = v.x
    wrv.y = v.y
    return wrv


def convert_vec3(v):
    wrv = et.WrVector3f()
    wrv.x = v.x
    wrv.y = v.y
    wrv.z = v.z
    return wrv


def convert_vec4(v):
    wrv = et.WrVector4f()
    wrv.x = v.x
    wrv.y = v.y
    wrv.z = v.z
    wrv.w = v.w
    return wrv


def convert_rgba(c):
    wrv = et.WrVector4f()
    wrv.x = c.r
    wrv.y = c.g
    wrv.z = c.b
    wrv.w = 1.0
    return wrv

def convert_mtx4x4(m):
    wrm = et.WrMatrix4x4()
    wrm.r0 = convert_vec4(m[0])
    wrm.r1 = convert_vec4(m[1])
    wrm.r2 = convert_vec4(m[2])
    wrm.r3 = convert_vec4(m[3])
    return wrm
