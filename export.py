##########################################################
# Copyright 2016 Sencer Cavus. All rights reserved.      #
# License: https://opensource.org/licenses/MIT           #
##########################################################

import ctypes
import json
import bpy
from mathutils import Vector
from . import util
from . import export_types as et

def export_scene(f, scene):
    meshes = []
    area_lights = []

    for ob in scene.objects:
        if ob.type == 'MESH':
            meshes.append(ob)
        elif ob.type == 'LAMP' and ob.data.type == 'AREA':
            area_lights.append(ob)

    header = et.WrSceneFileHeader()
    header.mesh_count = len(meshes)
    header.area_light_count = len(area_lights)
    f.write(header)
    export_camera(f, scene.camera)
    # export_materials()
    export_meshes(f, meshes)
    export_area_lights(f, area_lights)


# TODO: generate tangent frame if UVMap is absent
def export_meshes(f, meshes):
    for mesh_id, ob in enumerate(meshes):
        wrmesh = et.WrMesh()
        wrmesh.obj_to_world_mtx = util.convert_mtx4x4(ob.matrix_world)
        wrmesh.world_to_obj_mtx = util.convert_mtx4x4(ob.matrix_local)
        wrmesh.material_id = bpy.data.materials.keys().index(ob.active_material.name)
        mesh = ob.data
        mesh.calc_normals_split()
        #mesh.calc_tangents()
        wrmesh.polygon_count = 0
        tris = []

        for face in mesh.polygons:
            vtx_count = len(face.vertices)
            vertices = []

            for v, l in zip(face.vertices, face.loop_indices):
                vtx = et.WrVertex()
                vtx.pos = util.convert_vec3(mesh.vertices[v].co)
                normal = mesh.vertices[v].normal
                c1 = normal.cross(Vector([0.0, 0.0, 1.0]))
                c2 = normal.cross(Vector([0.0, 1.0, 0.0]))
                tangent = Vector()

                if c1.length > c2.length:
                    tangent = c1.normalized()
                else:
                    tangent = c2.normalized()

                binormal = normal.cross(tangent).normalized();
                vtx.normal = util.convert_vec3(mesh.vertices[v].normal)
                vtx.tangent = util.convert_vec3(tangent)
                vtx.binormal = util.convert_vec3(binormal)

                #vtx.tangent = util.convert_vec3(mesh.loops[l].tangent)
                #vtx.binormal = util.convert_vec3(mesh.loops[l].bitangent)
                #vtx.binormal = convert_vec3(mesh.loops[l].bitangent_sign * mesh.loops[l].normal.cross(mesh.loops[l].tangent))
                #vtx.texcoord = convert_vec2(mesh.uv_layers.active.data[l].uv)
                vertices.append(vtx)

            if vtx_count == 3:
                tri = et.WrTriangle()
                tri.vtx0 = vertices[0]
                tri.vtx1 = vertices[1]
                tri.vtx2 = vertices[2]
                tri.mesh_id = mesh_id
                tris.append(tri)
                wrmesh.polygon_count = wrmesh.polygon_count + 1

            if vtx_count == 4:
                tri0 = et.WrTriangle()
                tri1 = et.WrTriangle()
                tri0.vtx0 = vertices[0]
                tri0.vtx1 = vertices[1]
                tri0.vtx2 = vertices[2]
                tri0.mesh_id = mesh_id
                tri1.vtx0 = vertices[2]
                tri1.vtx1 = vertices[3]
                tri1.vtx2 = vertices[0]
                tri1.mesh_id = mesh_id
                tris.append(tri0)
                tris.append(tri1)
                wrmesh.polygon_count = wrmesh.polygon_count + 2

        f.write(wrmesh)

        for tri in tris:
            f.write(tri)

# TODO:
def export_materials(scene, ob):
    for mtl in bpy.data.materials:
        wrmtl
        wrscene.materials.append(wrmtl)


def export_textures():
    for img in bpy.data.images:
        return


def export_camera(f, camera):
    ob = camera
    cam = ob.data
    wrcam = et.WrCamera()
    wrcam.camera_to_world_mtx = util.convert_mtx4x4(ob.matrix_world)
    #wrcam.properties.pinhole.fov = degrees(cam.angle)
    wrcam.properties.pinhole.fov = cam.WrCamera.fov
    # TODO:
    wrcam.type = 0
    f.write(wrcam)


def export_area_lights(f, light_obs):
    for light_ob in light_obs:
        light = light_ob.data
        wrlight = et.WrAreaLight()
        wrlight.type = et.wr_area_light_types[light.WrLamp.WrLampArea.shape]
        wrlight.obj_to_world_mtx = util.convert_mtx4x4(light_ob.matrix_world)
        wrlight.world_to_obj_mtx = util.convert_mtx4x4(light_ob.matrix_local)
        wrlight.intensity = light.WrLamp.intensity
        wrlight.exposure = light.WrLamp.exposure
        wrlight.color = util.convert_rgba(light.color)
        f.write(wrlight)


def export_env_light(f, light):
    return


def export_render_settings(f, scene):
    integrator_type = scene.integrator_type_prop
    d = {}
    # TODO: get enabled device list
    if integrator_type == 'BRANCHED_DEFERRED_UDPT':
        d = {'enabled_cuda_devices': [0],
             'enabled_ocl_devices': [],
             'render_width': scene.render.resolution_x,
             'render_height': scene.render.resolution_y,
             'tile_width': scene.tile_width,
             'tile_height': scene.tile_height,
             'integrator_type': 'BranchedDeferredUdpt',
             'aa_sample_count': scene.bdudpt_aa_sample_count,
             'di_sample_count': scene.bdudpt_di_sample_count,
             'ii_sample_count': scene.bdudpt_ii_sample_count,
             'max_indirect_bounce_count': scene.bdudpt_max_indirect_bounce_count,
             'enable_mis': scene.bdudpt_enable_mis,
             'enable_rr': scene.bdudpt_enable_rr,
             'mesh_intersector_type': scene.mesh_intersector_type_prop,
             'area_light_intersector_type': scene.area_light_intersector_type_prop}
    elif integrator_type == 'PROGRESSIVE_DEFERRED_UDPT':
        d = {'enabled_cuda_devices': [0],
             'enabled_ocl_devices': [],
             'render_width': scene.render.resolution_x,
             'render_height': scene.render.resolution_y,
             'tile_width': scene.tile_width,
             'tile_height': scene.tile_height,
             'integrator_type': 'ProgressiveDeferredUdpt',
             'iteration_count': scene.pdudpt_iteration_count,
             'max_indirect_bounce_count': scene.pdudpt_max_indirect_bounce_count,
             'enable_mis': scene.pdudpt_enable_mis,
             'enable_rr': scene.pdudpt_enable_rr,
             'mesh_intersector_type': scene.mesh_intersector_type_prop,
             'area_light_intersector_type': scene.area_light_intersector_type_prop}

    json.dump(d, f)
