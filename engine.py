##########################################################
# Copyright 2016 Sencer Cavus. All rights reserved.      #
# License: https://opensource.org/licenses/MIT           #
##########################################################

import subprocess
import bpy
from . import common
from . import preferences
from . import export

debug_export_dir = ''

class WrRenderEngine(bpy.types.RenderEngine):
    bl_idname = common.default_bl_name
    bl_label = 'Wisard'
    bl_use_preview = True

    def update(self, data, scene):
        f = open(debug_export_dir + 'scene.wsf', 'wb')
        export.export_scene(f, scene)
        f.close()
        f = open(debug_export_dir + 'settings.json', 'w')
        export.export_render_settings(f, scene)
        f.close()
        return

    def render(self, scene):
        if scene.name == 'preview':
            self.render_preview(scene)
        else:
            self.render_scene(scene)

    def render_preview(self, scene):
        return

    def render_scene(self, scene):
        proc = subprocess.Popen([preferences.get_binary_path(), '-r', debug_export_dir + 'settings.json',
            '-s', debug_export_dir + 'scene.wsf', '-o', debug_export_dir + 'output.bmp'], cwd=preferences.get_binary_dir())
        proc.wait()
        return


def register():
    bpy.utils.register_class(WrRenderEngine)


def unregister():
    bpy.utils.unregister_class(WrRenderEngine)
