##########################################################
# Copyright 2016 Sencer Cavus. All rights reserved.      #
# License: https://opensource.org/licenses/MIT           #
##########################################################

import ctypes

class WrSceneFileHeader(ctypes.Structure):
    _fields_ = [('mesh_count', ctypes.c_uint32),
                ('area_light_count', ctypes.c_uint32)]


class WrVector2f(ctypes.Structure):
    _fields_ = [('x', ctypes.c_float),
                ('y', ctypes.c_float)]


class WrVector3f(ctypes.Structure):
    _fields_ = [('x', ctypes.c_float),
                ('y', ctypes.c_float),
                ('z', ctypes.c_float)]


class WrVector4f(ctypes.Structure):
    _fields_ = [('x', ctypes.c_float),
                ('y', ctypes.c_float),
                ('z', ctypes.c_float),
                ('w', ctypes.c_float)]


class WrMatrix4x4(ctypes.Structure):
    _fields_ = [('r0', WrVector4f),
                ('r1', WrVector4f),
                ('r2', WrVector4f),
                ('r3', WrVector4f)]


class WrVertex(ctypes.Structure):
	_fields_ = [('pos', WrVector3f),
			    ('normal', WrVector3f),
			    ('tangent', WrVector3f),
			   	('binormal', WrVector3f),
			   	('texcoord', WrVector2f)]


class WrTriangle(ctypes.Structure):
	_fields_ = [('vtx0', WrVertex),
				('vtx1', WrVertex),
				('vtx2', WrVertex),
				('mesh_id', ctypes.c_uint32)]


class WrPinholeCameraProperties(ctypes.Structure):
    _fields_ = [('fov', ctypes.c_float)]


class WrThinLensCameraProperties(ctypes.Structure):
    _fields_ = [('aperture_size', ctypes.c_float),
                ('focal_length', ctypes.c_float),
                ('fov', ctypes.c_float)]


class WrCameraProperties(ctypes.Union):
    _fields_ = [('pinhole', WrPinholeCameraProperties),
                ('thinlens', WrThinLensCameraProperties)]


class WrCamera(ctypes.Structure):
    _fields_ = [('type', ctypes.c_uint),
                ('properties', WrCameraProperties),
                ('camera_to_world_mtx', WrMatrix4x4)]


class WrMesh(ctypes.Structure):
    _fields_ = [('obj_to_world_mtx', WrMatrix4x4),
                ('world_to_obj_mtx', WrMatrix4x4),
                ('material_id', ctypes.c_uint32),
                ('polygon_count', ctypes.c_uint32)]


class WrPolygonSoup(ctypes.Structure):
    _fields_ = [('polygons', ctypes.POINTER(WrTriangle))]


wr_area_light_types = {'DISK': 0, 'SPHERE': 1, 'QUAD': 2}


class WrAreaLight(ctypes.Structure):
    _fields_ = [('type', ctypes.c_uint),
                ('obj_to_world_mtx', WrMatrix4x4),
                ('world_to_obj_mtx', WrMatrix4x4),
                ('intensity', ctypes.c_float),
                ('exposure', ctypes.c_float),
                ('color', WrVector4f)]
