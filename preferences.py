##########################################################
# Copyright 2016 Sencer Cavus. All rights reserved.      #
# License: https://opensource.org/licenses/MIT           #
##########################################################

import platform
import bpy
from extensions_framework import util as efutil
from . import common


class WrAddonPreferences(bpy.types.AddonPreferences):
    bl_idname = common.preferences_bl_name
    install_path = bpy.props.StringProperty(
        name = 'Path to Wisard Renderer binary',
        description = 'Path to Wisard Renderer binary',
        subtype = 'DIR_PATH')

    def draw(self, context):
        layout = self.layout
        layout.prop(self, 'install_path')


def get_binary_dir():
    addon_prefs = bpy.context.user_preferences.addons[common.preferences_bl_name].preferences
    return_path = addon_prefs.install_path
    return efutil.filesystem_path(return_path) + '/'


def get_binary_path():
    binary_dir = get_binary_dir()

    if platform.system() == 'Linux':
        return binary_dir + 'wr-cli'
    elif platform.system() == 'Windows':
        return binary_dir + 'wr-cli.exe'


def register():
    bpy.utils.register_class(WrAddonPreferences)


def unregister():
    bpy.utils.unregister_class(WrAddonPreferences)
